
import 'package:get/get.dart';
import 'package:getx_flutter/router/page.dart';
import 'package:getx_flutter/screen/home.binding.dart';
import 'package:getx_flutter/screen/home.screen.dart';

 final routers = [
    GetPage(name: HOME, page: () =>  HomeScreen(),binding: HomeBinding()),
    GetPage(name: HOMES, page: () => OtherScreen())
];