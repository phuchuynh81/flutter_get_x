import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_flutter/router/page.dart';
import 'package:getx_flutter/router/router.dart';

void main(){
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'GetX',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: HOME,
      getPages: routers,

    );
  }
}
