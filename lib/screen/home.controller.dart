import 'package:get/get.dart';

class HomeController extends GetxController {
  var counter = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }
  void increment(){
     counter ++;
  }
  void descrement(){
    counter --;
  }
}