import 'package:get/get.dart';
import 'package:getx_flutter/screen/home.controller.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(HomeController());
  }

}