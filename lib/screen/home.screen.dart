import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:getx_flutter/router/page.dart';
import 'package:getx_flutter/screen/home.controller.dart';

// bien counter nay la mot Rx , no hoat dong theo che Observe. De ben screen lang nghe duoc su thay doi cua b
// chung ta can phai boc no trong ham Obx()
class HomeScreen extends GetWidget<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          child: Column(
            children: [
              Obx(() => Text(
                    '${controller.counter}',
                    style: TextStyle(fontSize: 20),
                  )),
              SizedBox(
                height: 40,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "+",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  controller.increment();
                },
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "-",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  controller.descrement();
                },
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "GotoScreen",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () async {
                  Get.toNamed(HOMES, arguments: ['a','b']);
                },
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ),
      ),
    );
  }
}

class OtherScreen extends GetWidget<HomeController> {
  String val = '';

  OtherScreen({this.val});

  @override
  Widget build(BuildContext context) {
    print("phuchuynh:${Get.arguments[0]}");
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          child: Column(
            children: [
              Obx(() => Text(
                    '${controller.counter}',
                    style: TextStyle(fontSize: 20),
                  )),
              SizedBox(
                height: 40,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "+",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  controller.increment();
                },
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "-",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  controller.descrement();
                },
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                child: Container(
                  color: Colors.red,
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Text(
                      "GoBack",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  Get.back(result: 'success');
                },
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ),
      ),
    );
  }
}
